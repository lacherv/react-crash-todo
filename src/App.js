import React, { Component } from 'react';
import Todos from './components/Todos';

import './App.css';
class App extends Component {

  state = {
    todos: [
      {
        id: 1,
        title: 'Learn React',
        completed: true
      },
      {
        id: 2,
        title: 'Learn Redux',
        completed: false
      },
      {
        id: 3,
        title: 'Learn React Native',
        completed: false
      },
    ]
  };

  render() {    
    return (
      <div className="App">
        <Todos todos={this.state.todos} />
      </div>
    );
  }
}

export default App;

